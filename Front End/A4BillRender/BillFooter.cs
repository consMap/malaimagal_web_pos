﻿using System.Drawing;

namespace A4BillRender
{
    internal class BillFooter : Style
    {
        private writer RoundOff { get; set; }
        private writer TotalCGST { get; set; }
        private writer TotalSGST { get; set; }
        private writer TotalTax { get; set; }
        private writer AmountPayable { get; set; }
        private TwoLineWriter AmountInWords { get; set; }
        private TwoLineWriter TotalTaxInWords { get; set; }

        private class TwoLineWriter
        {
            public writer Line1 { get; set; }
            public writer Line2 { get; set; }
        }

        public BillFooter(RenderImage.Bill.Footer input)
        {
            RoundOff = new writer(16.30f, 24.25f, input.RoundOff, 20);
            TotalCGST = new writer(3.25f, 24.27f, input.TotalCGST, 22);
            TotalSGST = new writer(3.25f, 25.27f, input.TotalSGST, 22);
            TotalTax = new writer(3.25f, 26.27f, input.TotalTax, 24);
            AmountPayable = new writer(13.70f, 25.5f, input.AmountPayable, 14);
            TotalTaxInWords = new TwoLineWriter
            {
                Line1 = new writer(1.10f, 27.60f, input.TotalTaxInWords.Line1, 42),
                Line2 = new writer(1.10f, 28.05f, input.TotalTaxInWords.Line2, 42)
            };
            AmountInWords = new TwoLineWriter
            {
                Line1 = new writer(12.18f, 27.60f, input.AmountInWords.Line1, 49),
                Line2 = new writer(12.18f, 28.05f, input.AmountInWords.Line2, 49)
            };
        }

        public override void Render(Bitmap canvas)
        {
            var graphic = Graphics.FromImage(canvas);

            graphic.DrawString(RoundOff.Text, Calibri_10, Black, RoundOff.Position);
            graphic.DrawString(TotalCGST.Text, Calibri_10, Black, TotalCGST.Position);
            graphic.DrawString(TotalSGST.Text, Calibri_10, Black, TotalSGST.Position);
            graphic.DrawString(TotalTax.Text, Calibri_10, Black, TotalTax.Position);
            graphic.DrawString(AmountPayable.Text, Calibri_24, Black, AmountPayable.Position);

            graphic.DrawString(AmountInWords.Line1.Text, Calibri_10, Black, AmountInWords.Line1.Position);
            graphic.DrawString(AmountInWords.Line2.Text, Calibri_10, Black, AmountInWords.Line2.Position);

            graphic.DrawString(TotalTaxInWords.Line1.Text, Calibri_10, Black, TotalTaxInWords.Line1.Position);
            graphic.DrawString(TotalTaxInWords.Line2.Text, Calibri_10, Black, TotalTaxInWords.Line2.Position);

            graphic.Flush();
        }
    }
}
