﻿namespace PureSale.Api.Models
{
    public class ApiResult<ServiceResult>
    {
        public string Status { get; set; }
        public ServiceResult Data { get; set; }
    }
}