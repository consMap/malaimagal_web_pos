﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using PureSale.Api.Library.Access;

using ReadAccess = PureSale.Api.Library.Access.Read;
using InsertAccessRequest = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Access.Insert>;
using ModifyAccessRequest = PureSale.Api.Models.Access.Insert;
using DeleteAccessRequest = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Access.Delete>;

using ReadSpoc = PureSale.Api.Library.Spoc.Read;
using AddSpoc = PureSale.Api.Library.Spoc.Add;
using ModifySpoc = PureSale.Api.Library.Spoc.Modify;
using DeleteSpoc = PureSale.Api.Library.Spoc.Delete;
using ReadCustomer = PureSale.Api.Library.Customer.Read;
using AddCustomer = PureSale.Api.Library.Customer.Add;
using ModifyCustomer = PureSale.Api.Library.Customer.Modify;
using DeleteCustomer = PureSale.Api.Library.Customer.Delete;
using ClientRequest = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Client.Request>;
using SpocRequest = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Spoc.Schema>;
using CustomerRequest = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Customer.Request>;
using PureSale.Api.Models.Customer;

namespace PureSale.Api.Controllers
{
    [RoutePrefix("Users")]
    public class UserController : ApiController
    {
        //Todo : User, Vendor
        [HttpGet, Route("Access")]
        public IHttpActionResult GetAccess()
        {
            var service = new ReadAccess();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result()));
        }

        [HttpPost, Route("Access")]
        public IHttpActionResult AddAccess([FromBody] InsertAccessRequest model)
        {
            var service = new Add();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(model)));
        }

        [HttpPost, Route("Access")]
        public IHttpActionResult ModifyAccess([FromBody] ModifyAccessRequest model, string Id)
        {
            var service = new Modify();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id, model)));
        }

        [HttpPost, Route("Access/Delete")]
        public IHttpActionResult DeleteAccess([FromBody] DeleteAccessRequest Ids)
        {
            var service = new Delete();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Ids)));
        }

        [HttpGet, Route("Client")]
        public IHttpActionResult GetClient()
        {
            var service = new GetClient();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result()));
        }

        [HttpPost, Route("Client")]
        public IHttpActionResult AddClient([FromBody] ClientRequest model)
        {
            var service = new AddClient();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(model)));
        }

        [HttpPost, Route("Client")]
        public IHttpActionResult ModifyClient([FromBody] Models.Client.Request model, string Id)
        {
            var service = new ModifyClient();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id, model)));
        }

        [HttpGet, Route("Client/Delete")]
        public IHttpActionResult DeleteClient(string Id)
        {
            var service = new DeleteClient();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id)));
        }

        [HttpGet, Route("Spoc")]
        public IHttpActionResult GetSpoc()
        {
            var service = new ReadSpoc();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result()));
        }

        [HttpPost, Route("Spoc")]
        public IHttpActionResult AddSpoc([FromBody] SpocRequest model)
        {
            var service = new AddSpoc();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(model)));
        }

        [HttpPost, Route("Spoc")]
        public IHttpActionResult ModifySpoc([FromBody] Models.Spoc.Schema model, string Id)
        {
            var service = new ModifySpoc();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id, model)));
        }

        [HttpGet, Route("Spoc/Delete")]
        public IHttpActionResult DeleteSpoc(string Id)
        {
            var service = new DeleteSpoc();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id)));
        }

        [HttpGet, Route("Customer")]
        public IHttpActionResult GetCustomer()
        {
            var service = new ReadCustomer();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result()));
        }

        [HttpPost, Route("Customer")]
        public IHttpActionResult AddCustomer([FromBody] CustomerRequest model)
        {
            var service = new AddCustomer();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(model)));
        }

        [HttpPost, Route("Customer")]
        public IHttpActionResult ModifyCustomer([FromBody] Request model, string Id)
        {
            var service = new ModifyCustomer();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id, model)));
        }

        [HttpGet, Route("Customer/Delete")]
        public IHttpActionResult DeleteCustomer(string Id)
        {
            var service = new DeleteCustomer();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result(Id)));
        }
    }
}
