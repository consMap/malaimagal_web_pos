﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Spoc.Schema;

namespace PureSale.Api.Library.Spoc
{
    public class Modify : Update
    {
        private Model Model { get; set; }

        public Modify() : base("spoc")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id, Request request)
        {
            //AddMap(1, request.Name);
            //AddMap(2, request.Phone);
            UpdateRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}