﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Client.Request;

namespace PureSale.Api.Library.Access
{
    public class ModifyClient : Update
    {
        private Model Model { get; set; }

        public ModifyClient() : base("client")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id, Request request)
        {
            AddMap(1, request.Name);
            AddMap(2, request.BusinessName);
            AddMap(3, request.Contact);
            AddMap(4, request.Address);
            AddMap(5, request.SpocId); // todo : validate from spoc table
            UpdateRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}