﻿using System.Drawing;

namespace A4BillRender
{
    internal abstract class Style
    {
        public Font Calibri_10 { get; private set; }
        public Font Calibri_24 { get; private set; }
        public Brush Black { get; private set; }

        public Style()
        {
            Calibri_10 = new Font(new FontFamily("calibri"), 10.0f, FontStyle.Regular);
            Calibri_24 = new Font(new FontFamily("calibri"), 24.0f, FontStyle.Regular);
            Black = Brushes.Black;
        }

        public abstract void Render(Bitmap input);

        private static PointF penPosition(float x, float y)
        {
            var oneCm = 118.0952f;
            var xPen = x * oneCm;
            var yPen = y * oneCm;
            return new PointF(xPen, yPen);
        }

        public class writer
        {
            public PointF Position { get; private set; }
            public string Text { get; set; }

            public writer(float x, float y,string text, int charLimit)
            {
                Position = penPosition(x, y);
                Text = validateLength(text, charLimit);
            }
        }
        private static string validateLength(string input, int maxChars)
        {
            if (!string.IsNullOrEmpty(input) && input.Length > maxChars)
            {
                return $"{input.Substring(0, maxChars)}..";
            }
            return input;
        }
    }
}
