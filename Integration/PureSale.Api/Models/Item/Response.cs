﻿namespace PureSale.Api.Models.Item
{
    public class Response
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public Unit Unit { get; set; }
        public Quantity Quantity { get; set; }
        public Tax Tax { get; set; }
    }

    public class Quantity
    {
        public string InStock { get; set; }
        public string MinimumRequired { get; set; }
    }

    public class Unit
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class Tax
    {
        public string Sgst { get; set; }
        public string Cgst { get; set; }
        public string Igst { get; set; }
    }
}