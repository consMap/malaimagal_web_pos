﻿namespace PureSale.Web.Application.Library.ServiceModels.ClientPush
{
    public class Request
    {
        public string Message { get; set; }
    }
}
