﻿namespace PureSale.Api.Models
{
    public class ApiStatus
    {
        public const string Success = "success";
        public const string Error = "error";
        public const string Fail = "fail";
    }
}