﻿namespace PureSale.Api.Models.Spoc
{
    public class Request
    {
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}