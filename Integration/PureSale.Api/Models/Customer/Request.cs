﻿namespace PureSale.Api.Models.Customer
{
    public class Request
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string ClientId { get; set; }
    }
}