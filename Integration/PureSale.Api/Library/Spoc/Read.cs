﻿using PureSale.Api.Models.Spoc;
using System.Collections.Generic;
using Model = PureSale.Api.Models.Common.Table<PureSale.Api.Models.Spoc.Schema>;

namespace PureSale.Api.Library.Spoc
{
    public sealed class Read : Get
    {
        private Model Model { get; set; }

        private Schema Row { get; set; }

        public Read() : base("select * from spoc")
        {
            Model = new Model
            {
                Records = new List<Schema>()
            };
            AddMap(0, SetId);
            AddMap(1, SetName);
            AddMap(2, SetPhone);
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        private void SetId(string value)
        {
            Row = new Schema();
            Row.Id = value;
        }

        private void SetName(string value)
        {
            Row.Name = value;
        }

        private void SetPhone(string value)
        {
            Row.Phone = value;
            AddRecord();
        }

        private void AddRecord()
        {
            Model.Records.Add(Row);
        }

        public Model Result()
        {
            Populate();
            return Model;
        }
    }
}