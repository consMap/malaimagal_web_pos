﻿namespace PureSale.Api.Models.Product.Get
{
    public class Response
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}