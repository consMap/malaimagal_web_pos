﻿using System.Collections.Generic;

namespace PureSale.Api.Models.Common
{
    public class Table<Tschema>
    {
        public int RowsAffected { get; set; }
        public List<Tschema> Records { get; set; }
    }
}