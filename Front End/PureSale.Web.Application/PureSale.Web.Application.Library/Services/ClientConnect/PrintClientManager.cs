﻿using Microsoft.AspNet.SignalR;

namespace PureSale.Web.Application.Library.Services.ClientConnect
{
    public class PrintClientManager : Hub
    {
        public void LogClient(string id)
        {
            Clients.Caller.Verified($"Welcome {id} - You can now print.");
        }
    }
}
