﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PureSale.Windows.Console.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var uri = ConfigurationManager.AppSettings["EndPoint"];
            var service = new PrintMessageClient(uri, OnMessage);
            System.Console.WriteLine("Establishing Connection.... Please wait");
            Task.WaitAll(service.SayHello("123"));
            System.Console.ReadKey();
            System.Console.Clear();
            System.Console.WriteLine("Closing Connection.... Please wait");
            Thread.Sleep(500);
        }

        private static void OnMessage(string message)
        {
            try
            {
                var time = DateTime.Now.ToString("ddMMMyyyhhmmsss");
                var bytes = Convert.FromBase64String(message);
                var memoryStream = new MemoryStream(bytes);
                var bmp = new Bitmap(memoryStream);
                if (ConfigurationManager.AppSettings["BillCache"] != string.Empty)
                {
                    var path = ConfigurationManager.AppSettings["BillCache"];
                    var file = File.Open($"{path}/{time}.jpeg", FileMode.CreateNew);
                    memoryStream.WriteTo(file);
                    file.Close();
                }
                Print(bmp, time);
                System.Console.WriteLine(time);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error Occurred...");
                System.Console.WriteLine(ex.Message);
                System.Console.WriteLine(ex.StackTrace);
            }
        }

        static void Print(Image bill, string billName)
        {
            var setting = new PrinterSettings
            {
                PrintFileName = billName
            };
            var printer = new PrintDocument()
            {
                OriginAtMargins = true,
                DefaultPageSettings = new PageSettings()
                {
                    Margins = new Margins()
                    {
                        Left = 0,
                        Top = 0,
                        Right = 0,
                        Bottom = 0
                    },
                    PaperSize = new PaperSize() { RawKind = (int)PaperKind.A6 },
                    PrinterResolution = Convert.ToBoolean(ConfigurationManager.AppSettings["HighRes"]) ?
                    new PrinterResolution { Kind = PrinterResolutionKind.High } : new PrinterResolution { Kind = PrinterResolutionKind.Low },
                    Color = Convert.ToBoolean(ConfigurationManager.AppSettings["IsColor"]),
                },
                DocumentName = billName
            };
            printer.PrintPage += Printer_PrintPage;
            void Printer_PrintPage(object sender, PrintPageEventArgs e)
            {
                int width = Convert.ToInt32(printer.DefaultPageSettings.PrintableArea.Width);
                int height = Convert.ToInt32(printer.DefaultPageSettings.PrintableArea.Height);
                if (ConfigurationManager.AppSettings["PaperX"] != string.Empty && ConfigurationManager.AppSettings["PaperY"] != string.Empty)
                {
                    width = Convert.ToInt32(ConfigurationManager.AppSettings["PaperX"]);
                    height = Convert.ToInt32(ConfigurationManager.AppSettings["PaperY"]);
                }
                if (ConfigurationManager.AppSettings["PrintType"].Equals("Scaled", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Graphics.DrawImage(bill, setting.DefaultPageSettings.HardMarginX, setting.DefaultPageSettings.HardMarginY, width, height);
                }
                else if (ConfigurationManager.AppSettings["PrintType"].Equals("UnScaled", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Graphics.DrawImageUnscaled(bill, 0, 0, width, height);
                }
            }
            printer.Print();
        }
    }

    public class PrintMessageClient
    {
        public string Url { get; set; }
        public HubConnection Connection { get; set; }
        public IHubProxy Hub { get; set; }

        public PrintMessageClient(string url, Action<string> action)
        {
            Url = url;
            Connection = new HubConnection(url, useDefaultUrl: false);
            Hub = Connection.CreateHubProxy("PrintClientManager");
            Connection.Start().Wait();
            Hub.On<string>("Verified", (message)=> 
            {
                System.Console.WriteLine(message);
            });
            Hub.On<string>("PrintA6", action);
        }

        public async Task SayHello(string message)
        {
            await Hub.Invoke("LogClient", message);
            System.Console.WriteLine("Connected to server...");
        }

        public void Stop()
        {
            Connection.Stop();
        }
    }
}
