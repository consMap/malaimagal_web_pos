﻿using PureSale.Api.Models.Access;
using System.Collections.Generic;
using Model = PureSale.Api.Models.Common.Table<PureSale.Api.Models.Access.Schema>;

namespace PureSale.Api.Library.Access
{
    public sealed class Read : Get
    {
        private Model Model { get; set; }

        private string Id { get; set; }
        private string Level { get; set; }

        public Read() : base("select * from access")
        {
            Model = new Model
            {
                Records = new List<Schema>()
            };
            AddMap(0, SetId);
            AddMap(1, SetLevel);
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        private void SetId(string value)
        {
            Id = value;
        }

        private void SetLevel(string value)
        {
            Level = value;
            AddRecord();
        }

        private void AddRecord()
        {
            Model.Records.Add(new Schema
            {
                Id = Id,
                Level = Level
            });
        }

        public Model Result()
        {
            Populate();
            return Model;
        }
    }
}