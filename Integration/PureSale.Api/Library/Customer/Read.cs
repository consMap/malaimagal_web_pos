﻿using PureSale.Api.Models.Customer;
using System.Collections.Generic;
using Model = PureSale.Api.Models.Common.Table<PureSale.Api.Models.Customer.Schema>;

namespace PureSale.Api.Library.Customer
{
    public sealed class Read : Get
    {
        private Model Model { get; set; }

        private Schema Row { get; set; }

        public Read() : base("select * from customer")
        {
            Model = new Model
            {
                Records = new List<Schema>()
            };
            AddMap(0, SetPhone);
            AddMap(1, SetName);
            AddMap(2, SetClientId);
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        private void SetName(string value)
        {
            Row = new Schema();
            Row.Name = value;
        }

        private void SetClientId(string value)
        {
            Row.ClientId = value;
            AddRecord();
        }

        private void SetPhone(string value)
        {
            Row.PhoneNumber = value;
        }

        private void AddRecord()
        {
            Model.Records.Add(Row);
        }

        public Model Result()
        {
            Populate();
            return Model;
        }
    }
}