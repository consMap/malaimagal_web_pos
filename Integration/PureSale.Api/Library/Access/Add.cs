﻿using System.Linq;
using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Access.Insert>;

namespace PureSale.Api.Library.Access
{
    public sealed class Add : Update
    {
        private Model Model { get; set; }

        public Add() : base("access")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(Request request)
        {
            InsertCommand("insert into access (level) values ", request.Records.Select(record => record.Level).ToList());
            DoUpdate();
            return Model;
        }
    }
}