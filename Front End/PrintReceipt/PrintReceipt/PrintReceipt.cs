﻿using System.Linq;
using System.Drawing;
using System.Drawing.Printing;

namespace PrintReceipt
{
    public class PrintReceipt
    {
        PrintDocument Printer;
        Image Receipt;

        public PrintReceipt(string receiptPath)
        {
            Printer = new PrintDocument
            {
                OriginAtMargins = true,
                DefaultPageSettings = new PageSettings
                {
                    Margins = new Margins
                    {
                        Left = 0,
                        Top = 0,
                        Right = 0,
                        Bottom = 0
                    },
                    PaperSize = new PaperSize
                    {
                        RawKind = (int)PaperKind.A4
                    }
                },
                DocumentName = receiptPath.Split('\\').ToList().Last()
            };
            Receipt = Image.FromFile(receiptPath);
            Printer.PrintPage += Printer_PrintPage;
        }

        private void Printer_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(Receipt, 0, 0, 827, 1169);
        }

        public void Print()
        {
            Printer.Print();
        }
    }
}
