﻿namespace PureSale.Api.Models.Spoc
{
    public class Schema
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}