﻿using System.ComponentModel.DataAnnotations;

namespace PureSale.Api.Models.Access
{
    public class Insert
    {
        [Required]
        public string Level { get; set; }
    }
}