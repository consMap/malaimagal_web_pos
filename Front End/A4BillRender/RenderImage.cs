﻿using System.Collections.Generic;
using System.Drawing;

namespace A4BillRender
{
    public class RenderImage
    {
        private Bitmap Canvas { get; }
        private int MaxBillItems { get; }
        public class Bill
        {
            public Header header { get; set; }
            public Footer footer { get; set; }
            public List<Item> Items { get; set; }
            public class Header
            {
                public string Invoice { get; set; }
                public string Date { get; set; }
                public string GSTIN { get; set; }
                public string PaymentMethod { get; set; }
                public DeliveredToDetail DeliveredToDetails { get; set; }
                public BuyerDetail BuyerDetails { get; set; }

                public class DeliveredToDetail
                {
                    public string Line1 { get; set; }
                    public string Line2 { get; set; }
                    public string Line3 { get; set; }
                }

                public class BuyerDetail
                {
                    public string Line1 { get; set; }
                    public string Line2 { get; set; }
                    public string Line3 { get; set; }
                    public string Line4 { get; set; }
                    public string Line5 { get; set; }
                    public string Line6 { get; set; }
                }
            }
            public class Footer
            {
                public string RoundOff { get; set; }
                public string TotalCGST { get; set; }
                public string TotalSGST { get; set; }
                public string TotalTax { get; set; }
                public string AmountPayable { get; set; }
                public TwoLine AmountInWords { get; set; }
                public TwoLine TotalTaxInWords { get; set; }

                public class TwoLine
                {
                    public string Line1 { get; set; }
                    public string Line2 { get; set; }

                }
            }
            public class Item
            {
                public string Sno { get; set; }
                public string ItemName { get; set; }
                public string Hsn { get; set; }
                public string Qty { get; set; }
                public string Unit { get; set; }
                public string Rate { get; set; }
                public string GrossAmount { get; set; }
                public string GstPercentage { get; set; }
                public string CGST { get; set; }
                public string SGST { get; set; }
                public string NetAmount { get; set; }
            }
        }

        public RenderImage(string billTemplatePath)
        {
            Canvas = new Bitmap(billTemplatePath);
            MaxBillItems = 35;
        }

        public void Render(string path, Bill bilingDetails)
        {
            new BillHeader(bilingDetails.header).Render(Canvas);
            new BillFooter(bilingDetails.footer).Render(Canvas);
            renderBillItems(bilingDetails.Items);
            Canvas.Save(path);
        }

        private void renderBillItems(List<Bill.Item> bilingList)
        {
            var initialVerticalOffset = 6.10f;
            var listLength = bilingList.Count;
            for (int itemCount = 1; itemCount <= MaxBillItems && itemCount <= listLength; itemCount++)
            {
                new BillItem(initialVerticalOffset, bilingList[itemCount - 1]).Render(Canvas);
                initialVerticalOffset += 0.5f;
            }
        }


    }
}
