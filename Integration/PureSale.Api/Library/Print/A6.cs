﻿using PureSale.Api.Models.Print;
using PureSale.Api.Models.Print.A6;
using System;
using System.Drawing;
using System.IO;
using System.Web.Hosting;

namespace PureSale.Api.Library.Print
{
    public class A6
    {
        public PrintResult Result { get; private set; }

        private DailyBill Model { get; }
        private Graphics Canvas { get; }
        private Image Image { get; }

        public A6(DailyBill model)
        {
            Model = model;
            Image = Image.FromFile(HostingEnvironment.MapPath(@"~\App_Data\Template.png"));
            Canvas = Graphics.FromImage(Image);
        }

        public void Print()
        {
            DrawHeader();
            DrawBody();
            DrawFooter();
#if DEBUG
            var opath = HostingEnvironment.MapPath(@"~\App_Data\Test.png");
            Image.Save(opath);
#endif
            Result = new PrintResult
            {
                Paper = new Paper
                {
                    Height = Plotter.A6.PixelHeight.ToString(),
                    Width = Plotter.A6.PixelWidth.ToString(),
                    PixelPerMil = Plotter.A6.Density.ToString(),
                    Type = "A6"
                },
                ImageFormat = "png",
                ImageData = GetBytes(Image)
            };
        }

        private string GetBytes(Image image)
        {
            using (var memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                return Convert.ToBase64String(memoryStream.ToArray());
            }
        }

        private void DrawHeader()
        {
            Canvas.DrawString($"Bill #: {Model.BillNumber.PadLeft(5)}", Plotter.A6.Header.Font, Plotter.A6.Header.Brush, Plotter.A6.GetPosition(8, 21));
            var date = Model.Date ?? DateTime.Now.ToString("dd-MMM-yyyy");
            Canvas.DrawString($"{date}", Plotter.A6.Header.Font, Plotter.A6.Header.Brush, Plotter.A6.GetPosition(90, 21));
            Canvas.DrawString($"To : {Model.ClientName}", Plotter.A6.Header.Font, Plotter.A6.Header.Brush, Plotter.A6.GetPosition(8, 25));
            Canvas.DrawString($"Mobile : {Model.ClientNumber}", Plotter.A6.Header.Font, Plotter.A6.Header.Brush, Plotter.A6.GetPosition(80, 25));
            Canvas.DrawString($"{Model.Details}", Plotter.A6.Header.Font, Plotter.A6.Header.Brush, Plotter.A6.GetPosition(8, 28));
        }

        private void DrawBody()
        {
            Canvas.DrawString("S.no", Plotter.A6.Body.Title.Font, Plotter.A6.Body.Title.Brush, Plotter.A6.GetPosition(7, 32));
            Canvas.DrawString("Item Name", Plotter.A6.Body.Title.Font, Plotter.A6.Body.Title.Brush, Plotter.A6.GetPosition(14, 32));
            Canvas.DrawString("Qty", Plotter.A6.Body.Title.Font, Plotter.A6.Body.Title.Brush, Plotter.A6.GetPosition(70, 32));
            Canvas.DrawString("Rate", Plotter.A6.Body.Title.Font, Plotter.A6.Body.Title.Brush, Plotter.A6.GetPosition(79, 32));
            Canvas.DrawString("Amount", Plotter.A6.Body.Title.Font, Plotter.A6.Body.Title.Brush, Plotter.A6.GetPosition(93, 32));
            DrawItems(38);
        }

        private void DrawFooter()
        {
            Canvas.DrawString(Model.Amount, Plotter.A6.Footer.Amount.Font, Plotter.A6.Footer.Amount.Brush, Plotter.A6.GetPosition(76, 140));
            //todo : gst items
        }

        private void DrawItems(int yInitPos)
        {
            int serialNumber = 1;
            int yPos = yInitPos;
            foreach (var item in Model.Items)
            {
                Canvas.DrawString(serialNumber.ToString(), Plotter.A6.Body.Content.Font, Plotter.A6.Body.Content.Brush, Plotter.A6.GetPosition(7, yPos));
                Canvas.DrawString(Plotter.A6.Body.Content.Snip(item.Name), Plotter.A6.Body.Content.Font, Plotter.A6.Body.Content.Brush, Plotter.A6.GetPosition(14, yPos));
                Canvas.DrawString(item.Quantity, Plotter.A6.Body.Content.Font, Plotter.A6.Body.Content.Brush, Plotter.A6.GetPosition(70, yPos));
                Canvas.DrawString(item.Rate, Plotter.A6.Body.Content.Font, Plotter.A6.Body.Content.Brush, Plotter.A6.GetPosition(79, yPos));
                Canvas.DrawString(item.Amount, Plotter.A6.Body.Content.Font, Plotter.A6.Body.Content.Brush, Plotter.A6.GetPosition(93, yPos));
                serialNumber++;
                yPos += 4;
            }
        }
    }
}