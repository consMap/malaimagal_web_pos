﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(PureSale.Web.Application.Library.Services.ClientConnect.Startup))]

namespace PureSale.Web.Application.Library.Services.ClientConnect
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR("/live/Print", new Microsoft.AspNet.SignalR.HubConfiguration
            {
                EnableDetailedErrors = true
            });
        }
    }
}
