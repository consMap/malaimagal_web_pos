﻿using Model = PureSale.Api.Models.Common.NonScalar;

namespace PureSale.Api.Library.Spoc
{
    public class Delete : Update
    {
        private Model Model { get; set; }

        public Delete() : base("spoc")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id)
        {
            DeleteRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}