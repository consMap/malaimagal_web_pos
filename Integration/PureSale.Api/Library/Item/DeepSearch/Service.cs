﻿using PureSale.Api.Models;
using PureSale.Api.Models.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GetByNameService = PureSale.Api.Library.Item.GetByName.Service;

namespace PureSale.Api.Library.Item.DeepSearch
{
    public class Service : Database
    {
        public List<Response> Response { get; private set; }
        public ServiceError Error { get; private set; }
        public bool IsSuccess { get; private set; }

        private string ProductName { get; set; }
        private List<string> Layers { get; set; }

        class Store
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
            public int Score { get; set; }
            public Response Item { get; set; }
        }

        public Service(string productName)
        {
            ProductName = CurateSpaces(productName);
            SetLayers();
        }

        public void Read()
        {
            Response = new List<Response>();
            var store = new List<Store>();
            foreach (var layer in Layers)
            {
                var service = new GetByNameService(layer);
                service.Read();
                if(service.IsSuccess)
                {
                    foreach (var item in service.Response)
                    {
                        if(store.Any(x=>x.Id== item.Id))
                        {
                            store.First(x => x.Id == item.Id).Count++;
                        }
                        else
                        {
                            var count = 0;
                            foreach (var letter in RemoveSpaces(Layers[0]))
                            {
                                if(item.Name.IndexOf(letter.ToString(), StringComparison.OrdinalIgnoreCase) >= 0)
                                {
                                    count++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if(count == RemoveSpaces(Layers[0]).Length)
                            {
                                store.Add(new Store
                                {
                                    Count = 1,
                                    Id = item.Id,
                                    Name = item.Name,
                                    Item = item,
                                    Score = MatchScore(RemoveSpaces(Layers[0]), RemoveSpaces(item.Name))
                                });
                            }
                        }
                    }
                }
                else
                {
                    IsSuccess = false;
                    Error = service.Error;
                }
            }
            if(store.Count > 0)
            {
                store.OrderByDescending(x => x.Count).ThenByDescending(x=>x.Score);
                var treshold = Convert.ToInt32(Math.Round(Layers.Count*0.25, 0));
                foreach (var item in store)
                {
                    if(item.Count >= treshold)
                    {
                        Response.Add(item.Item);
                    }
                    else
                    {
                        break;
                    }
                }
                if(Response.Count>0)
                {
                    IsSuccess = true;

                }
                else
                {
                    IsSuccess = false;
                    Error = new ServiceError
                    {
                        Data = "The item could not be found in the database",
                        Message = "Not found"
                    };
                }
            }
            else
            {
                IsSuccess = false;
                Error = new ServiceError
                {
                    Data = "The item could not be found in the database",
                    Message = "Not found"
                };
            }
        }

        private int MatchScore(string source, string target)
        {
            source = RemoveSpaces(source);
            target = RemoveSpaces(target);
            var distance = LevensheinDistance(source, target);
            if(source.Length < target.Length)
            {
                return source.Length - target.Length + distance;
            }
            else
            {
                return target.Length - source.Length + distance;
            }
        }

        private int LevensheinDistance(string s, string t)
        {
            if (string.IsNullOrEmpty(s))
            {
                if (string.IsNullOrEmpty(t))
                    return 0;
                return t.Length;
            }

            if (string.IsNullOrEmpty(t))
            {
                return s.Length;
            }

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // initialize the top and right of the table to 0, 1, 2, ...
            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 1; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            return d[n, m];
        }

        private string CurateSpaces(string input)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            return regex.Replace(input, " ");
        }

        private string RemoveSpaces(string input)
        {
            return input.Replace(" ", string.Empty);
        }

        private void SetLayers()
        {
            Layers = new List<string>
            {
                ProductName
            };

            var words = ProductName.Split(' ');
            foreach (var word in words)
            {
                Layers.Add(word);
            }

            ProductName = RemoveSpaces(ProductName);

            for (int i = 0; i < ProductName.Length; i++)
            {
                Layers.Add(ProductName.Substring(0, ProductName.Length-i));

                //if (i % 3 == 0)
                //{
                //    if(ProductName.Length - i >=3)
                //    {
                //        Layers.Add(ProductName.Substring(i, 3));
                //    }
                //    else
                //    {
                //        Layers.Add(ProductName.Substring(i));
                //        break;
                //    }
                //}
            }
        }
    }
}