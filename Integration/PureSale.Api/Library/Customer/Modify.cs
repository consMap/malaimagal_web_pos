﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Customer.Request;

namespace PureSale.Api.Library.Customer
{
    public class Modify : Update
    {
        private Model Model { get; set; }

        public Modify() : base("customer")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id, Request request)
        {
            AddMap(1, request.Name);
            AddMap(2, request.ClientId);
            UpdateRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}