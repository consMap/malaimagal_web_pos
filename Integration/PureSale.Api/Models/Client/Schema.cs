﻿namespace PureSale.Api.Models.Client
{
    public class Schema
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string SpocId { get; set; }
    }
}