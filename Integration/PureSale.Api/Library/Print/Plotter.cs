﻿using System.Drawing;

namespace PureSale.Api.Library.Print
{
    public static class Plotter
    {
        public static class A6
        {
            public const int PixelWidth = 1240;
            public const int PixelHeight = 1748;
            public const int Density = PixelWidth / 105;

            public static class Header
            {
                public static Font Font = new Font("Microsoft Tai Le", 20, FontStyle.Regular);
                public static SolidBrush Brush = new SolidBrush(ColorTranslator.FromHtml("#000000"));
            }

            public static class Body
            {
                public static class Title
                {
                    public static Font Font = new Font("Microsoft Tai Le", 24, FontStyle.Underline);
                    public static SolidBrush Brush = new SolidBrush(ColorTranslator.FromHtml("#0033cc"));
                }
                public static class Content
                {
                    public static Font Font = new Font("Microsoft Tai Le", 24, FontStyle.Regular);
                    public static SolidBrush Brush = new SolidBrush(ColorTranslator.FromHtml("#0033cc"));
                    public static string Snip(string input)
                    {
                        if (input.Length >= 40)
                        {
                            return $"{input.Substring(0, 40)}...";
                        }
                        else
                        {
                            return input;
                        }
                    }
                }
            }

            public static class Footer
            {
                public static class Amount
                {
                    public static Font Font = new Font("Microsoft Tai Le", 28, FontStyle.Bold);
                    public static SolidBrush Brush = new SolidBrush(ColorTranslator.FromHtml("#0033cc"));
                }

                public static class Tax
                {
                    public static Font Font = new Font("Microsoft Tai Le", 18, FontStyle.Bold);
                    public static SolidBrush Brush = new SolidBrush(ColorTranslator.FromHtml("#0033cc"));
                }
            }

            public static Point GetPosition(int left, int top)
            {
                return new Point
                {
                    X = left * Density,
                    Y = top * Density
                };
            }
        }

    }
}