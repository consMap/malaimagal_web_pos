﻿using System.ComponentModel.DataAnnotations;

namespace PureSale.Api.Models.Product.Add
{
    public class Product
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Unit { get; set; }

        [Required]
        public string Tax { get; set; }
    }
}