﻿namespace PureSale.Api.Models.Access
{
    public class Schema
    {
        public string Id { get; set; }
        public string Level { get; set; }
    }
}