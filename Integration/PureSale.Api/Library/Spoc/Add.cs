﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Spoc.Schema>;

namespace PureSale.Api.Library.Spoc
{
    public sealed class Add : Update
    {
        private Model Model { get; set; }

        public Add() : base("spoc")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(Request request)
        {
            foreach (var record in request.Records)
            {
                NewRow[1] = record.Name;
                NewRow[2] = record.Phone;
                AddRow();
            }
            DoUpdate();
            return Model;
        }
    }
}