﻿using System.Collections.Generic;
using System.Data;

namespace PureSale.Api.Library
{
    public abstract class Update : Database
    {
        protected DataRow NewRow { get; }
        protected abstract void SetAffectedRows();

        private int RowIndex { get; set; }

        public Update(string tableName)
        {
        }

        protected void AddMap(int index, object value)
        {
        }

        protected void AddRow()
        {
        }

        protected void UpdateRow(int columnIndex, string keyValue)
        {
        }

        protected void DeleteRow(int columnIndex, string keyValue)
        {
        }

        protected void ModifyCommand<Tkey, Tval>(string baseCommand, List<KeyValuePair<string, Tval>> paramCollection, KeyValuePair<string, Tkey> key)
        {
            var command = baseCommand;
            for (int i = 0; i <= paramCollection.Count - 1; i++)
            {
                var paramName = $"@p{i}";
                command += $"{paramCollection[i].Key}={paramName}";
                Command.Parameters.AddWithValue(paramName, paramCollection[i].Value);
            }
            command += $" where {key.Key}={key.Value}";
            Command.CommandText = command;
        }

        protected void DeleteCommand<T>(string baseCommand, T[] keys)
        {
            var command = baseCommand;
            var maxIndex = keys.Length - 1;
            for (int i = 0; i <= maxIndex; i++)
            {
                command += "(";
                var paramName = $"@p{i}";
                command += paramName;
                Command.Parameters.AddWithValue(paramName, keys[i]);
                if (i != maxIndex)
                {
                    command += ",";
                }
                else
                {
                    command += ")";
                }
            }
            Command.CommandText = command;
        }

        protected void InsertCommand<T>(string baseCommand, params List<T>[] paramCollection)
        {
            var command = baseCommand;
            var columns = paramCollection.Length - 1;
            for (var i = 0; i <= columns; i++)
            {
                List<T> columnCollection = paramCollection[i];
                var colMaxIndex = columnCollection.Count - 1;
                command += "(";
                for (var j = 0; j <= colMaxIndex; j++)
                {
                    var paramName = $"@p{i}{j}";
                    command += paramName;
                    if (j != colMaxIndex)
                    {
                        command += ",";
                    }
                    else
                    {
                        command += ")";
                    }
                    Command.Parameters.AddWithValue(paramName, columnCollection[j]);
                }
                if (i != columns)
                {
                    command += ",";
                }
            }
            Command.CommandText = command;
        }

        protected void DoUpdate()
        {
            Execute(true);
            SetAffectedRows();
        }
    }
}