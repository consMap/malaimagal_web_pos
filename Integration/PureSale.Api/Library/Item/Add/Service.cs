﻿using PureSale.Api.Models;
using System;
using Request = System.Collections.Generic.List<PureSale.Api.Models.Item.Add.Item>;

namespace PureSale.Api.Library.Item.Add
{
    public class Service : Database
    {
        public bool IsSuccess { get; private set; }
        public ServiceError Error { get; private set; }

        Request Request { get; set; }
        public Service(Request request)
        {
            Request = request;
            IsSuccess = true;
            Error = new ServiceError();
        }

        public void Validate()
        {
            foreach (var item in Request)
            {
                Command.CommandText = "select count(products.id) from products where products.id = @productId;";
                Command.Parameters.AddWithValue("productId", item.ProductId);
                Execute();
                if(Convert.ToInt32(Table.Rows[0][0]) == 0)
                {
                    IsSuccess = false;
                    Error.Message = "Invalid Request";
                    Error.Data = "Product could not be found";
                    return;
                }
                else
                {
                    Clear();
                    Command.CommandText = "select count(items.price) from items where items.product_id = @productId AND items.price = @price;";
                    Command.Parameters.AddWithValue("productId", item.ProductId);
                    Command.Parameters.AddWithValue("price", item.Price);
                    Execute();
                    if (Convert.ToInt32(Table.Rows[0][0]) != 0)
                    {
                        IsSuccess = false;
                        Error.Message = "Invalid Request";
                        Error.Data = "An Item already exists with same price";
                        return;
                    }
                }
                Clear();
            }
        }

        public void Add()
        {
            Validate();
            if(!IsSuccess)
            {
                return;
            }
            int itemId;
            foreach (var item in Request)
            {
                Command.CommandText = "select max(id)+1 from items;";
                Execute();
                var dbProductId = Table.Rows[0][0].ToString();
                if (string.IsNullOrEmpty(dbProductId))
                {
                    itemId = 1;
                }
                else
                {
                    itemId = Convert.ToInt32(dbProductId);
                }
                Clear();
                Command.CommandText = "insert into items values (@itemId, @productId, @price, @qty, @minQty);";
                Command.Parameters.AddWithValue("itemId", itemId);
                Command.Parameters.AddWithValue("productId", item.ProductId);
                Command.Parameters.AddWithValue("price", item.Price);
                Command.Parameters.AddWithValue("qty", item.Quantity?.Available);
                Command.Parameters.AddWithValue("minQty", item.Quantity?.Minimum);
                Execute(true);
            }
        }
    }
}