﻿using PureSale.Api.Models.Client;
using System.Collections.Generic;
using Model = PureSale.Api.Models.Common.Table<PureSale.Api.Models.Client.Schema>;

namespace PureSale.Api.Library.Access
{
    public sealed class GetClient : Get
    {
        private Model Model { get; set; }

        private Schema Row { get; set; }

        public GetClient() : base("select * from client")
        {
            Model = new Model
            {
                Records = new List<Schema>()
            };
            AddMap(0, SetId);
            AddMap(1, SetName);
            AddMap(2, SetBusinessName);
            AddMap(3, SetContact);
            AddMap(4, SetAddress);
            AddMap(5, SetSpocId);
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        private void SetId(string value)
        {
            Row = new Schema();
            Row.Id = value;
        }

        private void SetName(string value)
        {
            Row.Name = value;
        }

        private void SetBusinessName(string value)
        {
            Row.BusinessName = value;
        }

        private void SetContact(string value)
        {
            Row.Contact = value;
        }

        private void SetAddress(string value)
        {
            Row.Address = value;
        }

        private void SetSpocId(string value)
        {
            Row.SpocId = value;
            AddRecord();
        }

        private void AddRecord()
        {
            Model.Records.Add(Row);
        }

        public Model Result()
        {
            Populate();
            return Model;
        }
    }
}