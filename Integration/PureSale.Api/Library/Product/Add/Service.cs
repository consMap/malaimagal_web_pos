﻿using PureSale.Api.Library.Static.Database;
using PureSale.Api.Models;
using System;
using Request = System.Collections.Generic.List<PureSale.Api.Models.Product.Add.Product>;

namespace PureSale.Api.Library.Product.Add
{
    public class Service : Database
    {
        public bool IsSuccess { get; private set; }
        public ServiceError Error { get; private set; }

        Request Request { get; }

        public Service(Request request)
        {
            Request = request;
            Error = new ServiceError();
        }

        private void Validate()
        {
            foreach (var product in Request)
            {
                if(Id.Unit.Check(product.Unit))
                {
                    if(Id.Tax.Check(product.Tax))
                    {
                        Command.CommandText = "select count(products.name) from products where products.name like @productName AND products.tax_id = @taxId AND products.units_id = @unitId;";
                        Command.Parameters.AddWithValue("productName", $"%{product.Name}%");
                        Command.Parameters.AddWithValue("taxId", $"%{Id.Tax.Translate(product.Tax)}%");
                        Command.Parameters.AddWithValue("unitId", $"%{Id.Unit.Translate(product.Unit)}%");
                        Execute();
                        var count = Convert.ToInt32(Table.Rows[0][0]);
                        if(count != 0)
                        {
                            Error.Message = "Invalid Request";
                            Error.Data = $"{product.Name} already exists";
                            IsSuccess = false;
                            return;
                        }
                        else
                        {
                            IsSuccess = true;
                        }
                        Clear();
                    }
                    else
                    {
                        Error.Message = "Invalid Request";
                        Error.Data = $"Tax ' {product.Tax} ' cannot be verified";
                        IsSuccess = false;
                        return;
                    }
                }
                else
                {
                    Error.Message = "Invalid Request";
                    Error.Data = $"Unit ' {product.Unit} ' cannot be verified";
                    IsSuccess = false;
                    return;
                }
            }
        }

        public void Update()
        {
            Validate();
            if(!IsSuccess)
            {
                return;
            }
            int productId;
            foreach (var product in Request)
            {
                Command.CommandText = "select max(id)+1 from products;";
                Execute();
                var dbProductId = Table.Rows[0][0].ToString();
                if(string.IsNullOrEmpty(dbProductId))
                {
                    productId = 1;
                }
                else
                {
                    productId = Convert.ToInt32(dbProductId);
                }
                Clear();
                Command.CommandText = "insert into products values(@productId,@name,@unitId,@taxId);";
                Command.Parameters.AddWithValue("@productId", productId);
                Command.Parameters.AddWithValue("@name", product.Name);
                Command.Parameters.AddWithValue("@unitId", Id.Unit.Translate(product.Unit));
                Command.Parameters.AddWithValue("@taxId", Id.Tax.Translate(product.Tax));
                Execute(true);
                Clear();
            }
        }
    }
}