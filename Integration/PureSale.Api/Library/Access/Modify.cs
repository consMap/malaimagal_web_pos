﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Insert = PureSale.Api.Models.Access.Insert;
using System.Collections.Generic;

namespace PureSale.Api.Library.Access
{
    public class Modify : Update
    {
        private Model Model { get; set; }

        public Modify() : base("access")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id, Insert request)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("level", request.Level)
            };
            ModifyCommand("update access set ", parameters, new KeyValuePair<string, string>("id", id));
            DoUpdate();
            return Model;
        }
    }
}