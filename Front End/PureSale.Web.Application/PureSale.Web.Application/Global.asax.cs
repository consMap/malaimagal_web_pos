﻿using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Routing;

namespace PureSale.Web.Application
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
