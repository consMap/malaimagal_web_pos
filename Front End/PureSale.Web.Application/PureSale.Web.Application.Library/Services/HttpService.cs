﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;

namespace PureSale.Web.Application.Library.Services
{
    public abstract class HttpService
    {
        protected string ResponseString { get; private set; }
        protected bool IsSuccess { get; private set; }
        protected HttpStatusCode StatusCode { get; private set; }

        protected void Execute(string requestUri, string requestBody = null, Action handleError = null)
        {
            var verb = requestBody != null ? HttpMethod.Post : HttpMethod.Get;
            var req = new HttpRequestMessage(verb, requestUri);
            if (requestBody != null)
            {
                req.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");
            }
            using (var client = new HttpClient
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiUri"])
            })
            {
                var result = client.SendAsync(req).Result;
                if (!result.IsSuccessStatusCode)
                {
                    StatusCode = result.StatusCode;
                    handleError?.Invoke();
                }
                else
                {
                    IsSuccess = true;
                    StatusCode = result.StatusCode;
                    ResponseString = result.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
}
