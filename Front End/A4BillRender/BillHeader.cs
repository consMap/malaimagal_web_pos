﻿using System.Drawing;

namespace A4BillRender
{
    internal class BillHeader : Style
    {
        private writer Invoice { get; set; }
        private writer Date { get; set; }
        private writer PaymentMode { get; set; }
        private DeliveredToDetails DeliveredTo { get; set; }
        private writer GSTIN { get; set; }
        private BuyerDetail BuyerDetails { get; set; }

        private class BuyerDetail
        {
            public writer Line1 { get; set; }
            public writer Line2 { get; set; }
            public writer Line3 { get; set; }
            public writer Line4 { get; set; }
            public writer Line5 { get; set; }
            public writer Line6 { get; set; }
        }

        private class DeliveredToDetails
        {
            public writer Line1 { get; set; }
            public writer Line2 { get; set; }
            public writer Line3 { get; set; }
        }

        public BillHeader(RenderImage.Bill.Header input)
        {
            Invoice = new writer(17, 0.98f, input.Invoice, 15);
            Date = new writer(16.0f, 1.668f, input.Date, 22);
            GSTIN = new writer(9.47f, 4.27f, input.GSTIN, 24);
            DeliveredTo = new DeliveredToDetails
            {
                Line1 = new writer(15.12f, 3.48f, input.DeliveredToDetails.Line1, 30),
                Line2 = new writer(15.12f, 3.95f, input.DeliveredToDetails.Line2, 30),
                Line3 = new writer(15.12f, 4.45f, input.DeliveredToDetails.Line3, 30)
            };
            PaymentMode = new writer(17.9f, 2.3f, input.PaymentMethod, 10);
            BuyerDetails = new BuyerDetail
            {
                Line1 = new writer(8.09f, 1.55f, input.BuyerDetails.Line1, 42),
                Line2 = new writer(8.09f, 2.0f, input.BuyerDetails.Line2, 42),
                Line3 = new writer(8.09f, 2.43f, input.BuyerDetails.Line3, 42),
                Line4 = new writer(8.09f, 2.9f, input.BuyerDetails.Line4, 42),
                Line5 = new writer(8.09f, 3.34f, input.BuyerDetails.Line5, 42),
                Line6 = new writer(8.09f, 3.76f, input.BuyerDetails.Line6, 42)
            };
        }

        public override void Render(Bitmap canvas)
        {
            var graphic = Graphics.FromImage(canvas);

            graphic.DrawString(Invoice.Text, Calibri_10, Black, Invoice.Position);
            graphic.DrawString(Date.Text, Calibri_10, Black, Date.Position);
            graphic.DrawString(PaymentMode.Text, Calibri_10, Black, PaymentMode.Position);
            graphic.DrawString(GSTIN.Text, Calibri_10, Black, GSTIN.Position);

            graphic.DrawString(DeliveredTo.Line1.Text, Calibri_10, Black, DeliveredTo.Line1.Position);
            graphic.DrawString(DeliveredTo.Line2.Text, Calibri_10, Black, DeliveredTo.Line2.Position);
            graphic.DrawString(DeliveredTo.Line3.Text, Calibri_10, Black, DeliveredTo.Line3.Position);

            graphic.DrawString(BuyerDetails.Line1.Text, Calibri_10, Black, BuyerDetails.Line1.Position);
            graphic.DrawString(BuyerDetails.Line2.Text, Calibri_10, Black, BuyerDetails.Line2.Position);
            graphic.DrawString(BuyerDetails.Line3.Text, Calibri_10, Black, BuyerDetails.Line3.Position);
            graphic.DrawString(BuyerDetails.Line4.Text, Calibri_10, Black, BuyerDetails.Line4.Position);
            graphic.DrawString(BuyerDetails.Line5.Text, Calibri_10, Black, BuyerDetails.Line5.Position);
            graphic.DrawString(BuyerDetails.Line6.Text, Calibri_10, Black, BuyerDetails.Line6.Position);

            graphic.Flush();
        }
    }
}
