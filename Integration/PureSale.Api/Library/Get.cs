﻿using System;
using System.Collections.Generic;

namespace PureSale.Api.Library
{
    public abstract class Get : Database
    {
        private List<ColumnDefinition> ColumnDefinitions { get; set; }
        protected abstract void SetAffectedRows();

        public Get(string command)
        {
            Command.CommandText = command;
            Execute();
            ColumnDefinitions = new List<ColumnDefinition>();
        }

        protected void AddMap(int id, Action<string> Mapper)
        {
            ColumnDefinitions.Add(new ColumnDefinition
            {
                Index = id,
                Map = Mapper
            });
        }

        protected void Populate()
        {
            for (int i = 0; i < RowsAffected; i++)
            {
                foreach (var column in ColumnDefinitions)
                {
                    column.Map(Table.Rows[i][column.Index].ToString());
                }
            }
            SetAffectedRows();
        }
    }

    class ColumnDefinition
    {
        public int Index { get; set; }
        public object Value { get; set; }
        public Action<string> Map { get; set; }
    }
}