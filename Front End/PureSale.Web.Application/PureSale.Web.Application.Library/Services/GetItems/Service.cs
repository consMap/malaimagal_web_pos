﻿using Newtonsoft.Json;
using PureSale.Web.Application.Library.Services.GetItems.Model;
using System;
using System.Collections.Generic;
using System.Net;
using ApiResponse = PureSale.Web.Application.Library.ServiceModels.GetItems.Response;

namespace PureSale.Web.Application.Library.Services.GetItems
{
    public class Service : HttpService
    {
        public List<ApiResponse> Execute(string productName)
        {
            Execute($"Items/{productName}?deep=true", null);
            var res = new List<ApiResponse>();
            if (!IsSuccess)
            {
                return res;
            }
            var response = JsonConvert.DeserializeObject<Response>(ResponseString);
            foreach (var item in response.Data)
            {
                res.Add(new ApiResponse
                {
                    Id = item.Id,
                    Name = item.Name,
                    Rate = Convert.ToDecimal(item.Price)
                });
            }
            return res;
        }
    }
}
