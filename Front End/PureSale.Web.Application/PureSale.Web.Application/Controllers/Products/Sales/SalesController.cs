﻿using System.Web.Mvc;

namespace PureSale.Web.Application.Controllers.Products.Sales
{
    public class SalesController : Controller
    {
        // GET: Sales
        [Route("Sale")]
        public ActionResult Index()
        {
            return View("Sales");
        }
    }
}