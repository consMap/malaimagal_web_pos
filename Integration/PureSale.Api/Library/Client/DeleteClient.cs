﻿using Model = PureSale.Api.Models.Common.NonScalar;

namespace PureSale.Api.Library.Access
{
    public class DeleteClient : Update
    {
        private Model Model { get; set; }

        public DeleteClient() : base("client")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id)
        {
            DeleteRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}