﻿using System.Collections.Generic;

namespace PureSale.Api.Library.Static.Database
{
    public static class Id
    {
        public static Dictionary<string, string> Tax { get; }
        public static Dictionary<string, string> Unit { get; }

        static Id()
        {
            Tax = new Dictionary<string, string>
            {
                { "5", "1" },
                { "12", "2" },
                { "18", "3" },
                { "28", "4" },
                { "off", "5" }
            };

            Unit = new Dictionary<string, string>
            {
                { "meter","1" },
                { "feet","2" },
                { "length","3" },
                { "square feet" ,"4"},
                { "cubic feet" ,"5" },
                { "dozen" ,"6" },
                { "box" ,"7" },
                { "roll" ,"8" },
                { "packet" ,"9" },
                { "litre" ,"10" },
                { "set" ,"11" },
                { "bundle" ,"12" },
                { "grams" ,"13" },
                { "kilo grams" ,"14" },
                { "pieces" ,"15" },
                { "bag" ,"16" },
                { "half bag" ,"17" },
                { "quarter bag" ,"18" },
                { "ton" ,"19" },
                { "bond" ,"20" },
                { "load" ,"21" },
                { "auto" ,"22" },
                { "coil" ,"23" },
            };
        }

        public static bool Check(this Dictionary<string,string> source, string input)
        {
            return source.ContainsKey(input);
        }

        public static string Translate(this Dictionary<string, string> source, string input)
        {
            return source[input];
        }
    }
}