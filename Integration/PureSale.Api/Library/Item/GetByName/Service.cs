﻿using PureSale.Api.Models;
using PureSale.Api.Models.Item;
using System.Collections.Generic;

namespace PureSale.Api.Library.Item.GetByName
{
    public sealed class Service : Database
    {
        public List<Response> Response { get; private set; }
        public ServiceError Error { get; private set; }
        public bool IsSuccess { get; private set; }

        private string ProductName { get; }


        public Service(string productName)
        {
            ProductName = $"%{productName}%";
        }

        public void Read()
        {
            Response = new List<Response>();
            Command.CommandText = "select products.name as ProductName,items.price as Price,items.quantity as Qty,items.minimum_quantity as MinStock, gst.cgst as CGST, gst.sgst as SGST, gst.igst as IGST, units.name as UnitName, units.short_name as UnitNotation, items.id as itemId from items inner join products on items.product_id = products.id inner join gst on products.tax_id = gst.id inner join units on products.units_id = units.id where products.name like @product_name;";
            Command.Parameters.AddWithValue("@product_name", ProductName);
            Execute();
            if (RowsAffected != 0)
            {
                for (int row = 0; row < RowsAffected; row++)
                {
                    Response.Add(new Response
                    {
                        Id = Table.Rows[row][9].ToString(),
                        Name = Table.Rows[row][0].ToString(),
                        Price = Table.Rows[row][1].ToString(),
                        Quantity = new Quantity
                        {
                            InStock = Table.Rows[row][2].ToString(),
                            MinimumRequired = Table.Rows[row][3].ToString()
                        },
                        Tax = new Tax
                        {
                            Cgst = Table.Rows[row][4].ToString(),
                            Sgst = Table.Rows[row][5].ToString(),
                            Igst = Table.Rows[row][6].ToString()
                        },
                        Unit = new Unit
                        {
                            Name = Table.Rows[row][7].ToString(),
                            ShortName = Table.Rows[row][8].ToString()
                        }
                    });
                }
                IsSuccess = true;
            }
            else
            {
                Error = new ServiceError
                {
                    Message = "Not found",
                    Data = "The item could not be found in the database"
                };
                IsSuccess = false;
            }
        }
    }
}