﻿using PureSale.Api.Models;
using PureSale.Api.Models.Product.Get;
using System.Collections.Generic;

namespace PureSale.Api.Library.Product.Get
{
    public sealed class Service : Database
    {
        public List<Response> Response { get; private set; }
        public ServiceError Error { get; private set; }
        public bool IsSuccess { get; private set; }

        private string ProductName { get; }


        public Service(string productName)
        {
            ProductName = productName;
        }

        public void Read()
        {
            Response = new List<Response>();
            Command.CommandText = "select * from products where products.name like @product_id;";
            Command.Parameters.AddWithValue("@product_id", $"%{ProductName}%");
            Execute();
            if (RowsAffected != 0)
            {
                for (int row = 0; row < RowsAffected; row++)
                {
                    Response.Add(new Response
                    {
                        Id = Table.Rows[row][0].ToString(),
                        Name = Table.Rows[row][1].ToString()
                    });
                }
                IsSuccess = true;
            }
            else
            {
                Error = new ServiceError
                {
                    Message = "Not found",
                    Data = "The product could not be found"
                };
                IsSuccess = false;
            }
        }
    }
}