﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PureSale.Api.Models.Common
{
    public class Request<TRequest>
    {
        [Required]
        public List<TRequest> Records { get; set; }
    }
}