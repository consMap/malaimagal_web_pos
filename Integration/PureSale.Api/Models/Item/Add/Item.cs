﻿using System.ComponentModel.DataAnnotations;

namespace PureSale.Api.Models.Item.Add
{
    public class Item
    {
        [Required]
        public string ProductId { get; set; }
        [Required]
        public string Price { get; set; }
        public Quantity Quantity { get; set; }
    }

    public class Quantity
    {
        public string Minimum { get; set; }
        public string Available { get; set; }
    }
}