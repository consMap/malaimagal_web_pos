﻿using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Web.Hosting;

namespace PureSale.Api.Library
{
    public class Database
    {
        protected DataTable Table { get; private set; }
        protected int RowsAffected { get; private set; }
        protected SQLiteCommand Command { get; set; }

        private SQLiteConnection Connection { get; set; }

        public Database()
        {
            var dbFile = ConfigurationManager.AppSettings.Get("DatabaseFile");
            var connectionString = $"data source = {HostingEnvironment.MapPath(@"~\App_Data\")}{dbFile}; version=3; foreign keys=true;";
            Connection = new SQLiteConnection(connectionString);
            Command = new SQLiteCommand(Connection);
            Table = new DataTable();
        }

        public void Execute(bool isUpdate = false)
        {
            if (!isUpdate)
            {
                Connection.Open();
                var reader = Command.ExecuteReader();
                Table.Load(reader);
                RowsAffected = Table.Rows.Count;
                Connection.Close();
            }
            else
            {
                Connection.Open();
                RowsAffected = Command.ExecuteNonQuery();
                Connection.Close();
            }
        }

        protected void Clear()
        {
            Table.Reset();
            Command.Parameters.Clear();
            RowsAffected = default(int);
        }
    }
}