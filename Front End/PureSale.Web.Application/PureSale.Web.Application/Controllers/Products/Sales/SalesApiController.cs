﻿using PureSale.Web.Application.Library.ServiceModels.Billing;
using System.Web.Http;
using QueryService = PureSale.Web.Application.Library.Services.GetItems.Service;
using A6BillService = PureSale.Web.Application.Library.Services.Billing.A6Print;
using System.Net.Http;
using System.Net;

namespace PureSale.Web.Application.Controllers.Products.Sales
{
    [RoutePrefix("Api")]
    public class SalesApiController : ApiController
    {
        [Route("QueryItems/{productName}"), HttpGet]
        public IHttpActionResult GetItems(string productName)
        {
            var service = new QueryService();
            return Ok(service.Execute(productName));
        }

        [Route("PrintBill/A6/{clientId}"), HttpPost]
        public IHttpActionResult PrintBill([FromUri]string clientId, [FromBody] A6 request)
        {
            var service = new A6BillService(clientId, request);
            service.Execute();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }
    }
}
