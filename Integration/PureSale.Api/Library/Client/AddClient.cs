﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Client.Request>;

namespace PureSale.Api.Library.Access
{
    public sealed class AddClient : Update
    {
        private Model Model { get; set; }

        public AddClient() : base("client")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(Request request)
        {
            foreach (var record in request.Records)
            {
                NewRow[1] = record.Name;
                NewRow[2] = record.BusinessName;
                NewRow[3] = record.Contact;
                NewRow[4] = record.Address;
                NewRow[5] = record.SpocId; // todo : validate from spoc table
                AddRow();
            }
            DoUpdate();
            return Model;
        }
    }
}