﻿using PureSale.Api.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AddItemRequest = System.Collections.Generic.List<PureSale.Api.Models.Item.Add.Item>;
using AddItemService = PureSale.Api.Library.Item.Add.Service;
using ItemByIdService = PureSale.Api.Library.Item.GetById.Service;
using ItemByNameService = PureSale.Api.Library.Item.GetByName.Service;
using ProductSearchService = PureSale.Api.Library.Product.Get.Service;
using ProductSearchRequest = PureSale.Api.Models.Product.Get.Request;
using ProductSearchResponse = System.Collections.Generic.List<PureSale.Api.Models.Product.Get.Response>;
using AddProductService = PureSale.Api.Library.Product.Add.Service;
using AddProductServiceRequest = System.Collections.Generic.List<PureSale.Api.Models.Product.Add.Product>;
using ItemServiceResponse = System.Collections.Generic.List<PureSale.Api.Models.Item.Response>;
using ItemByNameDeepSearch = PureSale.Api.Library.Item.DeepSearch.Service;
using System;

namespace PureSale.Api.Controllers
{
    public class ProductController : ApiController
    {
        [HttpGet, Route("Items/{query}")]
        public IHttpActionResult GetItems([FromUri] string query, string deep)
        {
            //todo : parse base64
            if(int.TryParse(query, out int id))
            {
                var service = new ItemByIdService(id);
                service.Read();
                if (service.IsSuccess)
                {
                    var result = new ApiResult<ItemServiceResponse>
                    {
                        Status = ApiStatus.Success,
                        Data = service.Response
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, result));
                }
                else
                {
                    var result = new ApiResult<ServiceError>
                    {
                        Status = ApiStatus.Fail,
                        Data = service.Error
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, result));
                }
            }
            else if(deep.Equals("true", StringComparison.InvariantCultureIgnoreCase))
            {
                var service = new ItemByNameDeepSearch(query);
                service.Read();
                if (service.IsSuccess)
                {
                    var result = new ApiResult<ItemServiceResponse>
                    {
                        Status = ApiStatus.Success,
                        Data = service.Response
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, result));
                }
                else
                {
                    var result = new ApiResult<ServiceError>
                    {
                        Status = ApiStatus.Fail,
                        Data = service.Error
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, result));
                }
            }
            else
            {
                var service = new ItemByNameService(query);
                service.Read();
                if (service.IsSuccess)
                {
                    var result = new ApiResult<ItemServiceResponse>
                    {
                        Status = ApiStatus.Success,
                        Data = service.Response
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, result));
                }
                else
                {
                    var result = new ApiResult<ServiceError>
                    {
                        Status = ApiStatus.Fail,
                        Data = service.Error
                    };
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, result));
                }
            }
        }

        [HttpPost, Route("Products")]
        public IHttpActionResult GetProducts([FromBody] ProductSearchRequest request)
        {
            var service = new ProductSearchService(request.Name);
            service.Read();
            if (service.IsSuccess)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, new ApiResult<ProductSearchResponse>
                {
                    Status = ApiStatus.Success,
                    Data = service.Response
                }));
            }
            else
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, new ApiResult<ServiceError>
                {
                    Status = ApiStatus.Fail,
                    Data = service.Error
                }));
            }
        }

        [HttpPost, Route("Products/Add")]
        public IHttpActionResult AddProducts([FromBody] AddProductServiceRequest request)
        {
            var service = new AddProductService(request);
            service.Update();
            if(service.IsSuccess)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, new ApiResult<string>
                {
                    Status = ApiStatus.Success,
                    Data = "Added all the products"
                }));
            }
            else
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, new ApiResult<ServiceError>
                {
                    Status = ApiStatus.Fail,
                    Data = service.Error
                }));
            }
        } 

        [HttpPost, Route("Items/Add")]
        public IHttpActionResult AddItems([FromBody] AddItemRequest request)
        {
            var service = new AddItemService(request);
            service.Add();
            if(service.IsSuccess)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, new ApiResult<string>
                {
                    Status = ApiStatus.Success,
                    Data = "Added all the items"
                }));
            }
            else
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, new ApiResult<ServiceError>
                {
                    Status = ApiStatus.Fail,
                    Data = service.Error
                }));
            }
        }
    }
}
