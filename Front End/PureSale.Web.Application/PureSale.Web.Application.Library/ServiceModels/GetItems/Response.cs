﻿using Newtonsoft.Json;

namespace PureSale.Web.Application.Library.ServiceModels.GetItems
{
    public class Response
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("rate")]
        public decimal Rate { get; set; }
    }
}
