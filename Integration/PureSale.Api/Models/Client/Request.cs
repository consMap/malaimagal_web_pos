﻿namespace PureSale.Api.Models.Client
{
    public class Request
    {
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public int SpocId { get; set; }
    }
}