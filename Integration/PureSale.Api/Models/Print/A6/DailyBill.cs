﻿using System.Collections.Generic;

namespace PureSale.Api.Models.Print.A6
{
    public class DailyBill
    {
        public string BillNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientNumber { get; set; }
        public string Details { get; set; }
        public string Date { get; set; }
        public List<Item> Items { get; set; }
        public string Discount { get; set; }
        public string Amount { get; set; }
        public string TotalCgst { get; set; }
        public string TotalSgst { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public string Rate { get; set; }
        public string Quantity { get; set; }
        public Gst Gst { get; set; }
        public string Amount { get; set; }
    }

    public class Gst
    {
        public string Hsn { get; set; }
        public Component Cgst { get; set; }
        public Component Sgst { get; set; }
    }

    public class Component
    {
        public string Percentage { get; set; }
        public string Amount { get; set; }
    }
}