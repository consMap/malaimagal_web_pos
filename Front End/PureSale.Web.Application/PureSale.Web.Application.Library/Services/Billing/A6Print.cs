﻿using Microsoft.AspNet.SignalR;
using PureSale.Web.Application.Library.ServiceModels.Billing;
using PureSale.Web.Application.Library.Services.Billing.Model;
using System;
using System.Linq;

namespace PureSale.Web.Application.Library.Services.Billing
{
    public class A6Print
    {
        private string ClientId { get; set; }
        private A6 Request { get; set; }
        private IHubContext Remote { get; set; }


        public A6Print(string clientId, A6 request)
        {
            ClientId = clientId;
            Request = request;
            Remote = GlobalHost.ConnectionManager.GetHubContext("PrintClientManager");
        }

        public void Execute()
        {
            var request = new A6Bill
            {
                Amount = Request.Amount,
                BillNumber = Request.BillNumber,
                ClientName = " ",
                ClientNumber = " ",
                Date = DateTime.Now.ToString("dd-MMM-yyyy"),
                Items = (from item in Request.Items select new Model.Item
                {
                    Name = item.Name,
                    Amount = item.Amount,
                    Quantity = item.Quantity,
                    Rate = item.Rate
                }).ToList()
            };
            var billService = new GetA6Bill();
            var bill = billService.Execute(request);
            Remote.Clients.All.PrintA6(bill);
        }
    }
}
