﻿using PureSale.Api.Library.Print;
using PureSale.Api.Models.Print.A6;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PureSale.Api.Controllers
{
    public class PrintController : ApiController
    {
        [HttpPost, Route("Print/A6")]
        public IHttpActionResult AddAccess([FromBody] DailyBill model)
        {
            var service = new A6(model);
            service.Print();
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, service.Result));
        }
    }
}
