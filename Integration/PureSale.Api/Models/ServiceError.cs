﻿namespace PureSale.Api.Models
{
    public class ServiceError
    {
        public string Message { get; set; }
        public string Data { get; set; }
    }
}