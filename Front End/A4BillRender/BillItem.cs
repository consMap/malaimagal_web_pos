﻿using System;
using System.Drawing;

namespace A4BillRender
{
    internal class BillItem : Style
    {
        private writer Sno { get; set; }
        private writer Item { get; set; }
        private writer Hsn { get; set; }
        private writer Qty { get; set; }
        private writer Unit { get; set; }
        private writer Rate { get; set; }
        private writer GrossAmount { get; set; }
        private writer GstPercentage { get; set; }
        private writer CGST { get; set; }
        private writer SGST { get; set; }
        private writer NetAmount { get; set; }

        public BillItem(float y, RenderImage.Bill.Item input)
        {
            Sno = new writer(1.18f, y, input.Sno, 3);
            Item = new writer(2.11f, y, input.ItemName, 36);
            Hsn = new writer(8.21f, y, input.Hsn, 10);
            Qty = new writer(10.11f, y, input.Qty, 5);
            Unit = new writer(11.01f, y, input.Unit, 5);
            Rate = new writer(12.21f, y, input.Rate, 7);
            GrossAmount = new writer(13.52f, y, input.GrossAmount, 8);
            GstPercentage = new writer(15.11f, y, input.GstPercentage, 5);
            CGST = new writer(16.02f, y, input.CGST, 5);
            SGST = new writer(17.02f, y, input.SGST, 5);
            NetAmount = new writer(18.28f, y, input.NetAmount, 8);
        }

        public override void Render(Bitmap canvas)
        {
            //6.4
            var graphic = Graphics.FromImage(canvas);

            graphic.DrawString(Sno.Text, Calibri_10, Black, Sno.Position);
            graphic.DrawString(Item.Text, Calibri_10, Black, Item.Position);
            graphic.DrawString(Hsn.Text, Calibri_10, Black, Hsn.Position);
            graphic.DrawString(Qty.Text, Calibri_10, Black, Qty.Position);
            graphic.DrawString(Unit.Text, Calibri_10, Black, Unit.Position);
            graphic.DrawString(Rate.Text, Calibri_10, Black, Rate.Position);
            graphic.DrawString(GrossAmount.Text, Calibri_10, Black, GrossAmount.Position);
            graphic.DrawString(GstPercentage.Text, Calibri_10, Black, GstPercentage.Position);
            graphic.DrawString(CGST.Text, Calibri_10, Black, CGST.Position);
            graphic.DrawString(SGST.Text, Calibri_10, Black, SGST.Position);
            graphic.DrawString(NetAmount.Text, Calibri_10, Black, NetAmount.Position);

            graphic.Flush();
        }
    }
}
