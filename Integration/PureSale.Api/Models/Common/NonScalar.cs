﻿namespace PureSale.Api.Models.Common
{
    public class NonScalar
    {
        public int RowsAffected { get; set; }
    }
}