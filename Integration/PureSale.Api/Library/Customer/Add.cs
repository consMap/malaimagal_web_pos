﻿using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Customer.Request>;

namespace PureSale.Api.Library.Customer
{
    public sealed class Add : Update
    {
        private Model Model { get; set; }

        public Add() : base("customer")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(Request request)
        {
            foreach (var record in request.Records)
            {
                NewRow[0] = record.PhoneNumber;
                NewRow[1] = record.Name;
                NewRow[2] = record.ClientId;
                AddRow();
            }
            DoUpdate();
            return Model;
        }
    }
}