﻿using Model = PureSale.Api.Models.Common.NonScalar;

namespace PureSale.Api.Library.Customer
{
    public class Delete : Update
    {
        private Model Model { get; set; }

        public Delete() : base("customer")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(string id)
        {
            DeleteRow(0, id);
            DoUpdate();
            return Model;
        }
    }
}