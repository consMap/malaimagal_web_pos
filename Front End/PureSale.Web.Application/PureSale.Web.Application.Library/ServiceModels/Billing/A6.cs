﻿using System.Collections.Generic;

namespace PureSale.Web.Application.Library.ServiceModels.Billing
{
    public class A6
    {
        public string BillNumber { get; set; }
        public List<Item> Items { get; set; }
        public string Amount { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public string Rate { get; set; }
        public string Quantity { get; set; }
        public string Amount { get; set; }
    }
}
