﻿using System.Linq;
using Model = PureSale.Api.Models.Common.NonScalar;
using Request = PureSale.Api.Models.Common.Request<PureSale.Api.Models.Access.Delete>;

namespace PureSale.Api.Library.Access
{
    public class Delete : Update
    {
        private Model Model { get; set; }

        public Delete() : base("access")
        {
            Model = new Model();
        }

        protected override void SetAffectedRows()
        {
            Model.RowsAffected = RowsAffected;
        }

        public Model Result(Request ids)
        {
            DeleteCommand("delete from access where id in ", ids.Records.Select(record => record.Id).ToArray());
            DoUpdate();
            return Model;
        }
    }
}