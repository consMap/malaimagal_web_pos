﻿using System.ComponentModel.DataAnnotations;

namespace PureSale.Api.Models.Product.Get
{
    public class Request
    {
        [Required]
        public string Name { get; set; }
    }
}