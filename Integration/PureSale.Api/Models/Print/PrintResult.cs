﻿namespace PureSale.Api.Models.Print
{
    public class PrintResult
    {
        public Paper Paper { get; set; }
        public string ImageFormat { get; set; }
        public string ImageData { get; set; }
    }

    public class Paper
    {
        public string Type { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string PixelPerMil { get; set; }
    }
}