﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using Bytescout.Spreadsheet;
using Newtonsoft.Json;

namespace Populate
{
    class Program
    {
        static void Main(string[] args)
        {
            var spreadsheet = new Spreadsheet();
            var rejected = new List<Reject>();
            spreadsheet.LoadFromFile("Pilot.xlsx", CacheType.Memory);
            var sheet = spreadsheet.Worksheets["Sheet1"];
            int count = 1;
            int blankCount = 0;
            for (int i = 1; i <= sheet.UsedRangeRowMax; i++)
            {
                var itemName = sheet.Rows[i][0].ValueAsString;
                var unit = sheet.Rows[i][4].ValueAsString;
                var price = sheet.Rows[i][5].ValueAsString;
                var gst = sheet.Rows[i][9].ValueAsString;

                itemName = itemName.Trim();

                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);
                itemName = regex.Replace(itemName, " ");

                //itemName = itemName.Replace("\"", "\\\"");

                if (!string.IsNullOrEmpty(itemName))
                {
                    if (!string.IsNullOrEmpty(price))
                    {
                        if (string.IsNullOrEmpty(gst))
                        {
                            gst = "off";
                        }
                        using (var client = new HttpClient()
                        {
                            BaseAddress = new Uri("http://localhost/PureSale.Api/")
                        })
                        {
                            var productReq = new HttpRequestMessage(HttpMethod.Post, "Products/Add");
                            var product = new CreateProduct
                            {
                                Name = itemName,
                                Tax = gst,
                                Unit = unit
                            };
                            var productJson = JsonConvert.SerializeObject(new List<CreateProduct> { product });
                            productReq.Content = new StringContent(productJson, Encoding.UTF8, "application/json");
                            var response = client.SendAsync(productReq).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var prIdReq = new HttpRequestMessage(HttpMethod.Post, $"Products");
                                var prIdJson = JsonConvert.SerializeObject(new CreateItem { Name = itemName });
                                prIdReq.Content = new StringContent(prIdJson, Encoding.UTF8, "application/json");
                                response = client.SendAsync(prIdReq).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var json = response.Content.ReadAsStringAsync().Result;
                                    var productId = JsonConvert.DeserializeObject<ApiResult<List<Product>>>(json).Data[0].Id;

                                    var itmReq = new HttpRequestMessage(HttpMethod.Post, "Items/Add");
                                    itmReq.Content = new StringContent($"[{{\"productid\":\"{productId}\",\"price\":\"{price}\"}}]", Encoding.UTF8, "application/json");
                                    response = client.SendAsync(itmReq).Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        Console.WriteLine(itemName);
                                    }
                                    else
                                    {
                                        var jsonD= response.Content.ReadAsStringAsync().Result;
                                        var error = JsonConvert.DeserializeObject<ApiResult<ErrorData>>(jsonD);
                                        rejected.Add(new Reject
                                        {
                                            Item = itemName,
                                            Reason = error.Data.Message + " - " + error.Data.Data
                                        });
                                    }
                                }
                                else
                                {
                                    var json = response.Content.ReadAsStringAsync().Result;
                                    var error = JsonConvert.DeserializeObject<ApiResult<ErrorData>>(json);
                                    rejected.Add(new Reject
                                    {
                                        Item = itemName,
                                        Reason = error.Data.Message + " - " + error.Data.Data
                                    });
                                }

                            }
                            else
                            {
                                var json = response.Content.ReadAsStringAsync().Result;
                                var error = JsonConvert.DeserializeObject<ApiResult<ErrorData>>(json);
                                rejected.Add(new Reject
                                {
                                    Item = itemName,
                                    Reason = error.Data.Message + " - " + error.Data.Data
                                });
                            }
                        }
                        count++;
                        blankCount = 0;
                    }
                    else
                    {
                        rejected.Add(new Reject
                        {
                            Item = itemName,
                            Reason = "No Rate found"
                        });
                    }
                }
                else
                {
                    if (blankCount > 120)
                    {
                        break;
                    }
                    blankCount++;
                }
            }
            Console.WriteLine($"Done - {count}");
            var html = new StringWriter();
            html.WriteLine("<html>");
            html.WriteLine("<body>");
            html.WriteLine("<style>table, th, td {  border: 1px solid black; } </style>");
            html.WriteLine("<table>");
            html.WriteLine("<tr>");
            html.WriteLine("<th>S.no</th>");
            html.WriteLine("<th>Item Name</th>");
            html.WriteLine("<th>Reason</th>");
            html.WriteLine("</tr>");
            var sno = 1;
            foreach (var item in rejected)
            {
                html.WriteLine("<tr>");
                html.WriteLine($"<td>{sno}</td>");
                html.WriteLine($"<td>{item.Item}</td>");
                html.WriteLine($"<td>{item.Reason}</td>");
                html.WriteLine("</tr>");
                sno++;
            }
            html.WriteLine("</table>");
            html.WriteLine("</body>");
            html.WriteLine("</html>");
            var file = File.CreateText("test.html");
            file.Write(html);
            file.Close();
            Console.ReadKey();
        }
    }

    public class CreateItem
    {
        public string Name { get; set; }
    }

    public class CreateProduct
    {
        public string Name { get; set; }
        public string Unit { get; set; }
        public string Tax { get; set; }
    }

    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class ApiResult<T>
    {
        public string Status { get; set; }
        public T Data { get; set; }
    }

    public class ErrorData
    {
        public string Message { get; set; }
        public string Data { get; set; }
    }

    public class Reject
    {
        public string Item { get; set; }
        public string Reason { get; set; }
    }
}
