﻿using System;
using Newtonsoft.Json;
using PureSale.Web.Application.Library.Services.Billing.Model;

namespace PureSale.Web.Application.Library.Services.Billing
{
    public class GetA6Bill : HttpService
    {
        public string Execute(A6Bill bill)
        {
            Execute("Print/A6", JsonConvert.SerializeObject(bill));
            if(!IsSuccess)
            {
                throw new Exception("Issues with the call");
            }
            var response = JsonConvert.DeserializeObject<PrintResult>(ResponseString);
            return response.ImageData;
        }
    }
}
